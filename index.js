const request = require('request')
const parseString = require('xml2js').parseString
const token = process.env.TELEGRAM_BOT_TOKEN || 'Set your TOKEN HERE'
const { Bot } = require('tgapi')
const bot = new Bot(token)
 
bot
  .getMe()
  .then(console.log)

function getPageContent (url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body)
      } else {
        reject(error)
      }
    })
  })
}

async function main () {
  let atomfeed = await getPageContent('https://about.gitlab.com/atom.xml')
  parseString(atomfeed, function (err, result) {
    var releases = result.feed.entry.filter(o => o.title[0].toLowerCase().includes('release'))
    console.log(releases)
  })
}

main()
